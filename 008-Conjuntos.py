def inter(uno, dos):
    print uno.intersection(dos)

def juntar(uno, dos):
    print uno.union(dos)

def diferencia(uno, dos):
    print uno.difference(dos)

if __name__ == '__main__':
    uno = set()
    uno.add("pepe")
    uno.add("juliam")
    uno.add("Marcos")
    uno.add("Ivan")
    uno.add("Fernando")

    dos = set()
    dos.add("Maria")
    dos.add("Carlota")
    dos.add("Fernando")
    dos.add("pepe")
    dos.add("PIPERO")

    print uno
    print dos
    p= raw_input("Selecciona: i, j o d")
    if p == "i":
        inter(uno, dos)
    elif p == "j":
        juntar(uno, dos)
    elif p == "d":
        diferencia(uno, dos)
    else:
        print "No te he entendido: "
        print p